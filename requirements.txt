streamlit==1.29.0
pandas==1.3.3
numpy==1.21.2
altair==4.0
openpyxl
requests
BeautifulSoup4
streamlit_extras