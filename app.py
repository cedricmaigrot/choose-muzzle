import os

import streamlit as st
import pandas as pd
import numpy as np

st.set_page_config(layout="wide")

# Titre de l'application
st.title("Choix d'une muselière")


@st.cache_data
def load_dataframe():
    data = pd.read_excel("muzzles.xlsx")
    return data


# Charger le DataFrame
df = load_dataframe()
# with st.modal("Voir les détails d'une bonne muselière", "?") as m:
#     cols = st.columns(2)
#     with cols[0]:
#         st.image("illustration.jpg", caption="Sunrise by the mountains")
#         st.write(
#             "A : Le chien devrait pouvoir ouvrir la gueule avec le museau de manière à permettre une respiration et une consommation d'eau non entravées. En particulier par temps chaud ou lorsque le chien est soumis à une forte activité physique, comme lors d'activités sportives, il est important de veiller à ce que le chien puisse réguler sa température corporelle en haletant."
#         )
#         st.write(
#             "B : Le museau ne doit pas reposer sur le nez (également appelé museau) du chien. À l'extrémité du nez du chien, il devrait y avoir un espace d'environ 0,5 à 2 cm jusqu'à la fin du museau, en fonction du modèle et de la taille du chien, afin que le chien n'entre pas en contact avec l'avant de son museau."
#         )
#     with cols[1]:
#         st.write(
#             "C : Le museau devrait être en contact avec les joues en fonction du modèle, mais ne devrait pas les comprimer. Cependant, dans certains modèles, il est important que le museau soit en contact avec les joues, car cela assure la stabilité et empêche le museau de glisser d'un côté à l'autre. Donc : contact oui, compression non."
#         )
#         st.write(
#             "D : Le museau ne doit pas exercer de pression sur les yeux. Cependant, veuillez noter qu'un museau qui permet une respiration adéquate peut, logiquement, remonter automatiquement et se positionner devant ou sur les yeux lorsque le chien est couché avec la tête sur le sol."
#         )
#         st.write(
#             "E : Le museau ne doit pas exercer de pression sur la trachée et le larynx lorsque la tête du chien est dans une position normale. Cependant, lorsque le chien abaisse la tête, il peut arriver que le museau frotte contre le cou."
#         )
#         st.write(
#             "F : Le champ de vision du chien devrait être aussi peu restreint que possible. Cependant, chez certains chiens, les yeux sont situés si bas sur la tête et au niveau du museau qu'une certaine restriction est inévitable."
#         )
#         st.write(
#             "G : En fonction du modèle et de la durée/objectif de port du museau, il est important de s'assurer qu'il y a suffisamment de rembourrage."
#         )
#         st.write(
#             "H : Le museau ne devrait pas pouvoir être tiré vers le haut par le chien au-dessus du nez. Vous pouvez le vérifier en tirant une fois le museau vers l'avant en direction du nez. Le museau devrait bouger, mais ne pas pouvoir être retiré par-dessus le nez. Pour les chiens au museau court, une sangle frontale peut être nécessaire."
#         )

# with st.expander("Comment prendre les mesures"):
#     cols = st.columns(4)
#     with cols[0]:
#         st.image("dessus.jpg", caption="Vue du dessus")
#     with cols[1]:
#         st.subheader("Longueur (en rose)")
#         st.write("Prendre la mesure à 1 ou 2 cm des yeux jusqu'au bout du museau.")
#         st.subheader("Largeur (en vert)")
#         st.write("Prendre la mesure d'un coté à l'autre de la machoire.")
#     with cols[2]:
#         st.image("cote.jpg", caption="Vue de coté")
#     with cols[3]:
#         st.subheader("Hauteur (en jaune)")
#         st.write(
#             "Prendre la mesure avec une balle dans la gueule du haut du museau à l'extrémité de la machoire inférieure."
#         )

# Sidebar pour les entrées utilisateur
st.sidebar.subheader("Saisissez les mesures :")
longueur = st.sidebar.number_input("Longueur :", min_value=10.0)
# circonference = st.sidebar.number_input("Circonférence :", min_value=0.0)
largeur = st.sidebar.number_input("Largeur :", min_value=10.0)
hauteur = st.sidebar.number_input("Hauteur gueule ouverte :", min_value=10.0)
calcul = st.sidebar.selectbox(
    "Calcul de similarité",
    (
        "Seulement positif",
        "Au plus proche",
    ),
)


# Calculer le produit vectoriel pour chaque ligne du DataFrame
def calculate_similarity(row):
    vector = np.array([longueur, largeur, hauteur])
    data_vector = np.array(
        [
            row["Longueur"],
            # row["Circonférence"],
            row["Largeur"],
            row["Hauteur coté ouvert"],
            # row["Hauteur coté fermé"],
        ]
    )
    similarity = np.dot(vector, data_vector) / (
        np.linalg.norm(vector) * np.linalg.norm(data_vector)
    )
    return similarity


# Trouver les noms les plus proches en utilisant le produit vectoriel
df["similarity"] = df.apply(calculate_similarity, axis=1)
df.fillna(0, inplace=True)
sorted_df = df[df["similarity"] != 0]
sorted_df = sorted_df.sort_values(by="similarity", ascending=False)

# Appliquer la fonction de couleur aux colonnes de différences
col_names = [
    "Diff longueur",
    "Diff largeur",
    "Diff hauteur",
]


# Afficher les noms les plus proches avec des couleurs
sorted_df["Diff longueur"] = sorted_df.apply(lambda x: x["Longueur"] - longueur, axis=1)
sorted_df["Diff largeur"] = sorted_df.apply(lambda x: x["Largeur"] - largeur, axis=1)
sorted_df["Diff hauteur"] = sorted_df.apply(lambda x: x["Hauteur coté ouvert"] - hauteur, axis=1)

if calcul == "Seulement positif":
    for col in ["Diff longueur", "Diff largeur", "Diff hauteur"]:
        sorted_df = sorted_df[sorted_df[col] > 0]

nb_results = len(sorted_df)
styled_df = sorted_df.rename(columns={"Nom": "Modèle", "Sexe": "Variante"})[
    ["Modèle", "Marque", "Prix", "url"]
    + col_names
    + ["Longueur", "Circonférence", "Largeur", "Hauteur coté ouvert", "Hauteur coté fermé"]
]
st.subheader(f"Muselières les plus proches :")

styled_df.reset_index(False, inplace=True)

# st.markdown("""---""")
from streamlit_extras.card import card
from streamlit_extras.badges import badge

cols = st.columns(2)

for key, row in styled_df.head(12).iterrows():
    print(key)
    with cols[key % len(cols)]:
        card(
            key=key,
            title=row["Modèle"],
            text=[
                f"Longueur : {row['Longueur']} cm | Largeur : {row['Largeur']} cm",
                f"Hauteur ouvert : {row['Hauteur coté ouvert']} cm | Hauteur fermé : {row['Hauteur coté fermé']} cm",
                f"Circonférence : {row['Circonférence']} cm",
                f"---",
                f"{row['Marque']} | {row['Prix']}",
            ],
            url=row["url"],
            styles={
                "card": {
                    "width": "100%",
                    "height": "auto",
                    "min-height": "200px",
                    "padding": "20px",
                    "margin": "0px",
                }
            },
        )

st.dataframe(
    styled_df[
        [
            "Modèle",
            "Marque",
            "Longueur",
            "Largeur",
            "Hauteur coté ouvert",
            "Hauteur coté fermé",
            "Circonférence",
        ]
    ].head(12),
    use_container_width=True,
)
