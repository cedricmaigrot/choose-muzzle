import requests
from bs4 import BeautifulSoup
import pandas as pd


breeds = {
    "American Cocker": "Cocker américain",
    "Boxer": "Boxer",
    "Bullterrier": "Bullterrier",
    "Dobermann": "Dobermann",
    "Dogge": "Dogue allemand",
    "Foxterrier": "Fox-terrier",
    "Labrador": "Labrador",
    "Malinois": "Berger belge Malinois",
    "Mittelschnauzer": "Schnauzer moyen",
    "Neufundländer": "Terre-neuve",
    "Pitbull": "American Stafforshire Terrier",
    "Pudel": "Caniche",
    "Ridgeback": "Rhodesian Ridgeback",
    "Riesenschnauzer": "Schnauzer géant",
    "Rottweiler": "Rottweiler",
    "Sarplaninac": "Chien de berger illyrien",
    "Schäferhund": "Berger allemand (Mâle)",
    "Schäferhund Mittel": "Berger allemand moyen (Mâle)",
    "Schäferhund Sport": "Berger Allemand Mâle Sport (Mâle)",
    "Schäferhündin": "Berger allemand (Femelle)",
    "Schäferhündin Sport": "Berger Allemand Mâle Sport (Femelle)",
    "Spitz": "Spitz",
    "Zwergschnauzer": "Schnauzer nain",
}


def get_size(url):
    response = requests.get(url)
    soup = BeautifulSoup(response.text, "html.parser")

    # Recherchez tous les éléments <p> à l'intérieur de la balise active

    paragraphs = soup.find_all("div", class_="tab-body active")

    # Créez un dictionnaire pour stocker les tailles
    tailles_dict = []
    first_word = ""

    # Parcourez les paragraphes pour extraire les tailles
    for paragraph in paragraphs:
        items = (
            paragraph.text.split("Gewicht des Maulkorbs")[0]
            .split("Maulkörbe sind Handarbeit)")[-1]
            .strip()
            .split("\n")
        )
        first_word = paragraph.text.split(" ")[0].strip()

        if len(items) == 5:
            tailles_dict = [
                float(x.split(":")[-1].split("cm")[0].replace(",", ".").strip()) for x in items
            ]

    return tailles_dict, first_word


def get_name(reference):
    s = reference.replace("CHOPO Drahtmaulkorb", "")
    s = s.replace(" Rüde", "")
    s = s.replace(" Hündin", "").strip()
    if s in breeds:
        s = breeds[s]
    return s


def get_sex(reference):
    if " Rüde" in reference or "Schäferhund" in reference:
        return "Mâle"
    if " Hündin" in reference or "Schäferhündin" in reference:
        return "Femelle"
    return "Neutre"


def collect(url):
    response = requests.get(url)

    # Vérifier si la requête s'est bien déroulée
    if response.status_code == 200:
        soup = BeautifulSoup(response.text, "html.parser")

        # Supprimer les espaces vides et les retours à la ligne dans le texte
        clean_text = " ".join(soup.stripped_strings)

        # Vous devrez analyser la structure HTML de la page pour extraire les informations spécifiques que vous souhaitez.
        # Par exemple, supposons que les informations des produits sont dans des éléments <div> avec la classe "product".
        product_divs = soup.find_all("div", class_="product-container")

        # Créer une liste pour stocker les données des produits
        products_data = []

        for product_div in product_divs:
            # Vous pouvez extraire les détails du produit ici, par exemple :
            product_name = product_div.find("a", class_="product-url").text.strip()
            product_url = product_div.find("a", class_="product-url")["href"]
            product_price = product_div.find("span", class_="current-price-container").text.strip()
            sizes, first_word_of_description = get_size(product_url)

            marque = "Unknown"
            for item in ["Champion", "DINO", "CHOPO", "C&S"]:
                if product_name.startswith(item):
                    marque = item
            for item in ["IDEAL", "Maulkorbfactory"]:
                if first_word_of_description == item:
                    marque = item
            if "-JVM-" in url:
                marque = "JVM"

            if len(sizes) != 5:
                print("Il n'y a pas 5 tailles", product_name, product_url)
                continue

            values = {
                "Marque": marque,
                "url": product_url,
                "Référence": product_name,
                "Nom": get_name(product_name),
                "Sexe": get_sex(product_name),
                "Prix": product_price,
                "Longueur": sizes[0],
                "Circonférence": sizes[1],
                "Largeur": sizes[2],
                "Hauteur coté ouvert": sizes[3],
                "Hauteur coté fermé": sizes[4],
            }

            products_data.append(values)
        # Créer un DataFrame à partir des données des produits
        df = pd.DataFrame(products_data)
        return df
    else:
        print("La requête HTTP a échoué.")
        raise Exception()
