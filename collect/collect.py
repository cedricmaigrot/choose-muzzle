import pandas as pd

import functions as f

sources = [
    # Länge 2 - 5,9 cm
    "https://www.chicundscharf.com/?view_mode=default&cat=c4_Laenge-2---5-9-cm-Kurze-Schnauzen--0-6-cm-.html&listing_sort=&filter_id=0&listing_count=96",
    # Länge 6 - 8,9 cm
    "https://www.chicundscharf.com/?view_mode=default&cat=c5_Laenge-6---8-9-cm-Mittellange-Schnauzen--6-10-cm-.html&listing_sort=&filter_id=0&listing_count=96",
    # Länge 9 - 10,9 cm
    "https://www.chicundscharf.com/?view_mode=default&cat=c6_Laenge-9---10-9-cm-Lange-Schnauzen--10-cm---.html&listing_sort=&filter_id=0&listing_count=96",
    # Länge ab 11 cm
    "https://www.chicundscharf.com/?view_mode=default&cat=c143_Laenge-ab-11-cm-schnauzenlaenge-ab-11-cm.html&listing_sort=&filter_id=0&listing_count=96",
    # CHOPO Drahtmaulkörbe -> Verzinkt
    "https://www.chicundscharf.com/?view_mode=default&cat=c22_Verzinkt-Verzinkt.html&listing_sort=&filter_id=0&listing_count=96",
    # CHOPO Drahtmaulkörbe -> Schwarz ummantelt
    "https://www.chicundscharf.com/?view_mode=default&cat=c23_Schwarz-ummantelt-Schwarz-ummantelt.html&listing_sort=&filter_id=0&listing_count=96",
    # CHOPO Drahtmaulkörbe -> Verzinkt Biothane
    "https://www.chicundscharf.com/?view_mode=default&cat=c157_Verzinkt-Biothane-Verzinkt-157.html&listing_sort=&filter_id=0&listing_count=96",
    # CHOPO Drahtmaulkörbe -> Schwarz ummantelt Biothane
    "https://www.chicundscharf.com/?view_mode=default&cat=c158_Schwarz-ummantelt-Biothane-Schwarz-ummantelt-158.html&listing_sort=&filter_id=0&listing_count=96",
    # Safety First / JVM Drahtmaulkörbe -> Safety First / JVM Drahtmaulkörbe verzinkt
    "https://www.chicundscharf.com/?view_mode=default&cat=c115_Safety-First---JVM-Drahtmaulkoerbe-verzinkt-safety-first-drahtmaulkoerbe-verzinkt.html&listing_sort=&filter_id=0&listing_count=96",
    # Safety First / JVM Drahtmaulkörbe -> Safety First / JVM Drahtmaulkörbe schwarz ummantelt
    "https://www.chicundscharf.com/?view_mode=default&cat=c116_Safety-First---JVM-Drahtmaulkoerbe-schwarz-ummantelt-safety-first-drahtmaulkoerbe-schwarz-ummantelt.html&listing_sort=&filter_id=0&listing_count=96",
    # Bunte Drahtmaulkörbe
    "https://www.chicundscharf.com/?view_mode=default&cat=c20_Bunte-Drahtmaulkoerbe-Bunte-Drahtmaulkoerbe.html&listing_sort=&filter_id=0&listing_count=96",
    # Rohlinge pulverbeschichtet
    "https://www.chicundscharf.com/?view_mode=default&cat=c156_Rohlinge-pulverbeschichtet-rohlinge-pulverbeschichtet.html&listing_sort=&listing_count=96",
]

dfs = []
for url in sources:
    dfs.append(f.collect(url=url))

pd.concat(dfs).to_excel("../muzzles.xlsx")
